create database bimay

use bimay


create table Student(
	StudentId int identity(1,1) primary key not null,
	StudentName Varchar(255) not null,
	StudentAddress varchar(255) not null,
	StudentPhone varchar(255) not null,
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table Teacher(
	TeacherId int identity(1,1) primary key not null,
	TeacherName varchar(255) not null,
	TeacherAddress varchar(255) not null,
	TeacherEmail varchar(255) not null,
	StudentId int foreign key references Student(StudentId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table Message(
	MessageId int identity(1,1) primary key not null,
	StudentId int foreign key references Student(StudentId),
	TeacherId int foreign key references Teacher(TeacherId),
	MessageName varchar(255),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table MessageDetail(
	MessageDetailId int identity(1,1) primary key not null,
	MessageId int foreign key references Message(MessageId),
	MessageTitle varchar(255),
	MessageDescription varchar(255),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table Finance(
	FinanceId int identity(1,1) primary key not null,
	FinanceTotal int not null,
	StudentId int foreign key references Student(StudentId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table Schedule(
	ScheduleId int identity(1,1) primary key not null,
	ScheduleTime date,
	StudentId int foreign key references Student(StudentId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table ExamSchedule(
	ExamScheduleId int identity(1,1) primary key not null,
	ScheduleId int foreign key references Schedule(ScheduleId),
	ExamScheduleDate date,
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'

)


create table Curriculum(
	CurriculumId int identity(1,1) primary key not null,
	CurriculumName varchar(255) not null,
	ScheduleId int foreign key references Schedule(ScheduleId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table Forum(
	ForumId int identity(1,1) primary key not null,
	CurriculumId int foreign key references Curriculum(CurriculumId),
	TeacherId int foreign key references Teacher(TeacherId),
	ForumName varchar(255),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table ForumDetail(
	ForumDetailId int identity(1,1) primary key not null,
	ForumId int foreign key references Forum(ForumId),
	ForumTitle varchar(255),
	ForumDescription varchar(255),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'


)

create table Score(
	ScoreId int identity(1,1) primary key not null,
	ScoreYears date,
	StudentId int foreign key references Student(StudentId),
	CurriculumId int foreign key references Curriculum(CurriculumId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'

)

create table Feedback(
	FeedbackId int identity(1,1) primary key not null,
	FeedbackMessages varchar(255) not null,
	FeedbackDate date,
	StudentId int foreign key references Student(StudentId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

create table DetailFeedback(
	DetailFeedbackId int identity(1,1) primary key not null,
	Title varchar(255) not null,
	Descriptions varchar(255) not null,
	FeedbackId int foreign key references Feedback(FeedbackId),
	CreatedAt datetimeoffset Default GetDate(),
	CreateBy varchar(255) default 'Admin',
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin'
)

drop table DetailFeedback,Feedback,Score,ForumDetail,Forum,Curriculum,ExamSchedule,Schedule,Finance,Teacher,Student
