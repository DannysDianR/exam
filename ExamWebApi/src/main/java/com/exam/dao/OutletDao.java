package com.exam.dao;

import com.exam.model.Outlet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutletDao extends JpaRepository<Outlet, Long> {

}
