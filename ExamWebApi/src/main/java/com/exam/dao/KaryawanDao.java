package com.exam.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exam.model.*;


@Repository
public interface KaryawanDao extends JpaRepository<Karyawan, Long> {
	List<Karyawan> findByOutletId(Long outletid);
}
