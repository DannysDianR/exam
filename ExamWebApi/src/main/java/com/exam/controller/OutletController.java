package com.exam.controller;

import javax.validation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.exam.dao.OutletDao;
import com.exam.exception.NotFound;
import com.exam.model.Outlet;

@RestController
public class OutletController {
	
	@Autowired
	private OutletDao outletDao;
	
	@GetMapping("/Outlet")
	public Page<Outlet> getOutlet(Pageable pageable){
		return outletDao.findAll(pageable);
	}
	
	@PostMapping("/Outlet")
	public Outlet createOutlet(@Valid @RequestBody Outlet outletReq) {
		return outletDao.save(outletReq);
	}
	
	@PutMapping("/Outlet/{OutletId}")
	public Outlet updateOutlet(@PathVariable Long OutletId, @Valid @RequestBody Outlet outletReq) {
		return outletDao.findById(OutletId).map(outlet -> {
			outlet.setOutlettempat(outletReq.getOutlettempat());
			return outletDao.save(outlet);
		}).orElseThrow(() -> new NotFound("Outlet tidak ditemukan dengan Id = " + OutletId));
	}
	
	@DeleteMapping("/Outlet/{OutletId}")
	public ResponseEntity<?> deleteOutlet(@PathVariable Long OutletId){
		return outletDao.findById(OutletId).map(outlet -> {
			outletDao.delete(outlet);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new NotFound("Outlet tidak ditemukan dengan Id = " + OutletId));
	}

}
