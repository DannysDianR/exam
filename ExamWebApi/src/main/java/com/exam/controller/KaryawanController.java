package com.exam.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exam.dao.KaryawanDao;
import com.exam.dao.OutletDao;
import com.exam.model.Karyawan;

import com.exam.exception.*;

@RestController
public class KaryawanController {

	@Autowired
	private KaryawanDao karyawanDao;
	
	@Autowired
	private OutletDao outletDao;
	
	@GetMapping("/Outlet/{OutletId}/Karyawan")
	public List<Karyawan> getKaryawanByOutletId(@PathVariable Long OutletId){
		return karyawanDao.findByOutletId(OutletId);
	}
	
	@PostMapping("/Outlet/{OutletId}/Karyawan")
	public Karyawan addKaryawan(@PathVariable Long OutletId, @Valid @RequestBody Karyawan karyawanReq) {
		return outletDao.findById(OutletId).map(outlet -> {
			karyawanReq.setOutlet(outlet);
			return karyawanDao.save(karyawanReq);
		}).orElseThrow(()-> new NotFound("Outlet tidak dapat ditemukan dengan Id = " + OutletId));
	}
	
	@PutMapping("/Outlet/{OutletId}/Karyawan/{KaryawanId}")
	public Karyawan updateKaryawan(@PathVariable Long OutletId,@PathVariable Long KaryawanId,
									@Valid @RequestBody Karyawan karyawanReq) {
		if(!outletDao.existsById(OutletId)) {
			throw new NotFound("Not Found " + OutletId);
		}
		
		return karyawanDao.findById(KaryawanId).map(karyawan-> {
			karyawan.setNama(karyawanReq.getNama());
			karyawan.setAlamat(karyawanReq.getAlamat());
			karyawan.setNoHp(karyawanReq.getNoHp());
			karyawan.setEmergencyContact(karyawanReq.getEmergencyContact());
			karyawan.setGaji(karyawanReq.getGaji());
			karyawan.setStartKerja(karyawanReq.getStartKerja());
			karyawan.setResign(karyawanReq.getResign());
			return karyawanDao.save(karyawan);
			
		}).orElseThrow(()-> new NotFound("Not Found" + KaryawanId));
	}
	
	@DeleteMapping("/Outlet/{OutletId}/Karyawan/{KaryawanId}")
	public ResponseEntity<?> deleteKaryawan(@PathVariable Long OutletId, @PathVariable Long KaryawanId){
		if(!outletDao.existsById(OutletId)) {
			throw new NotFound("Not Found " + OutletId);
		}
		
		return karyawanDao.findById(KaryawanId).map(karyawan->{
			karyawanDao.delete(karyawan);
			return ResponseEntity.ok().build();
		}).orElseThrow(()-> new NotFound("Not Found" + KaryawanId));
	}
	
	
	
}
