package com.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.exam")
@EntityScan("com.exam.model")
@EnableJpaRepositories("com.exam.dao")
public class ExamWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamWebApiApplication.class, args);
	}

}
