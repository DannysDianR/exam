package com.exam.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
@Table(name = "Karyawan")
@DynamicInsert
public class Karyawan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long karyawanid;
	
	@Size(min = 5, max = 30, message = "Nama Anda harus diantara 10 - 30 Karakter, Terimakasih")
	@Column(name = "NamaKaryawan")
	private String nama;

	@Column(name = "AlamatRumah")
	private String alamat;

	@Column(name = "NoTelepon")
	private String noHp;

	@Column(name = "NoTeleponDarurat", columnDefinition = "Varchar(255) Default 'NULL'",nullable = true)
	private String emergencyContact;

	@Column(name = "Gaji")
	private String gaji;

	@Column(name = "MulaiKerja",columnDefinition = "Timestamp")
	private Date startKerja;

	@Column(name = "Resign", columnDefinition = "Timestamp")
	private Date resign;

	@Column(name = "CreateAt", columnDefinition = "Timestamp Default Now()")
	private Date createAt;

	@Column(name = "CreateBy", columnDefinition = "Varchar(255) Default 'SYSTEM'")
	private String createBy;

	@Column(name = "UpdateAt", columnDefinition = "Timestamp Default Now()")
	private Date updateAt;

	@Column(name = "UpdateBy", columnDefinition = "Varchar(255) Default 'SYSTEM'")
	private String updateBy;
	
	@ManyToOne
	@JoinColumn(name = "id",nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Outlet outlet;

	public Long getKaryawanid() {
		return karyawanid;
	}

	public void setKaryawanid(Long karyawanid) {
		this.karyawanid = karyawanid;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getGaji() {
		return gaji;
	}

	public void setGaji(String gaji) {
		this.gaji = gaji;
	}

	public Date getStartKerja() {
		return startKerja;
	}

	public void setStartKerja(Date startKerja) {
		this.startKerja = startKerja;
	}

	public Date getResign() {
		return resign;
	}

	public void setResign(Date resign) {
		this.resign = resign;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Outlet getOutlet() {
		return outlet;
	}

	public void setOutlet(Outlet outlet) {
		this.outlet = outlet;
	}
	
	
	
}
