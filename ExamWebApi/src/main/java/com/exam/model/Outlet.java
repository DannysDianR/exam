package com.exam.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "Outlet")
@DynamicInsert
public class Outlet implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "Outlet")
	private String outlettempat;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOutlettempat() {
		return outlettempat;
	}

	public void setOutlettempat(String outlettempat) {
		this.outlettempat = outlettempat;
	}


	
	
}
